import { useQuery } from '@tanstack/react-query';

import { ROOT } from '@/config';
import { authAPI } from '@/lib/axios';

interface AuthURLResponse {
  auth_url: string;
}

async function getAuthUrl(): Promise<AuthURLResponse> {
  try {
    const loginUrl = await authAPI.get<AuthURLResponse>(`/login?redirect_uri=${ROOT}/callback`);
    return loginUrl.data;
  } catch (error) {
    throw Error('Something went wrong.');
  }
}

export const useAuthUrl = () =>
  useQuery<AuthURLResponse>({
    queryKey: ['auth'],
    queryFn: () => getAuthUrl()
  });

/**
 * Redirect user to login via SKA-IAM. After successful login, the user is
 * redirected to the /callback route. This route runs the login callback
 * functions, which parse the PCKE code from the URL and fetch the user's
 * access token.
 */
export async function loginRedirect() {
  // Login via SKA IAM and redirect to LoginCallbackPage
  window.location.href = (await getAuthUrl()).auth_url;
}

export function logout() {
  localStorage.removeItem('maltopuft-token'); // Clear the token
  window.location.href = ROOT;
}
