import { useQuery } from '@tanstack/react-query';

import { Label } from '../types';

import { api } from '@/lib/axios';

export async function getLabelsByCandidateId(candidateIds: number[]): Promise<Label[]> {
  const searchParams = new URLSearchParams();
  candidateIds.forEach((id: number) => searchParams.append('candidate_id', id.toString()));

  if (candidateIds.length > 0) {
    searchParams.append('limit', candidateIds.length.toString());
  }

  try {
    const response = await api.get<Label[]>('/labels', { params: searchParams });
    return response.data.map((d) => ({
      id: d.id,
      labeller_id: d.labeller_id,
      candidate_id: d.candidate_id,
      entity_id: d.entity_id
    }));
  } catch (error) {
    throw Error('Something went wrong.');
  }
}

export const useLabels = (candidateIds: number[]) =>
  useQuery<Label[]>({
    queryKey: ['labels', 'get', candidateIds],
    queryFn: () => getLabelsByCandidateId(candidateIds)
  });
