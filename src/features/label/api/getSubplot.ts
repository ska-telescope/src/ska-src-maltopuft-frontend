import { useQuery } from '@tanstack/react-query';

import { SinglePulse, SubplotData } from '../types';

import { api } from '@/lib/axios';

export async function getSubplot(sps: SinglePulse[]): Promise<SubplotData[]> {
  const searchParams = new URLSearchParams();
  sps.forEach((sp) => {
    searchParams.append('plot_path', sp.plot_path);
  });

  try {
    const response = await api.get<Record<string, string>>('/fs/download', {
      params: searchParams
    });
    return sps.map((sp: SinglePulse) => {
      const imageData = response.data[sp.plot_path];
      return {
        candidate_id: sp.candidate_id,
        plot_path: sp.plot_path,
        imageData: `data:${response.headers['content-type']};base64,${imageData}`
      };
    });
  } catch (error) {
    throw Error('Something went wrong.');
  }
}

export const useSubplots = (sps: SinglePulse[]) =>
  useQuery<SubplotData[], Error>({
    // swap key for obs_id and pageNumber?
    queryKey: ['subplot', 'get', sps.map((sp) => sp.candidate_id)],
    queryFn: () => getSubplot(sps)
  });
