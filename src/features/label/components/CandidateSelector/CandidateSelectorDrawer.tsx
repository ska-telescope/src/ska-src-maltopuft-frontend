import MenuOpenIcon from '@mui/icons-material/MenuOpen';
import { IconButton } from '@mui/material';
import Box from '@mui/material/Box';
import Drawer from '@mui/material/Drawer';
import { useState } from 'react';

import ObservationDateRangePicker from './ObservationDateRangePicker';

import ToggleButton from '@/components/ToggleButton';
import dayjs from '@/lib/dayjs';

interface CandidateSelectorProps {
  fetchLatest: boolean;
  setFetchLatest: React.Dispatch<React.SetStateAction<boolean>>;
  startTime: dayjs.Dayjs;
  setStartTime: React.Dispatch<React.SetStateAction<dayjs.Dayjs>>;
  endTime: dayjs.Dayjs;
  setEndTime: React.Dispatch<React.SetStateAction<dayjs.Dayjs>>;
}

function CandidateSelector({ ...props }: CandidateSelectorProps) {
  return (
    <Box sx={{ width: 265 }} role="presentation">
      <ToggleButton
        isToggled={props.fetchLatest}
        setIsToggled={props.setFetchLatest}
        buttonLabel="Latest observation"
      />
      <ObservationDateRangePicker
        startTime={props.startTime}
        setStartTime={props.setStartTime}
        endTime={props.endTime}
        setEndTime={props.setEndTime}
        fetchLatest={props.fetchLatest}
      />
    </Box>
  );
}

function CandidateSelectorDrawer({ ...props }: CandidateSelectorProps) {
  const [isOpen, setIsOpen] = useState<boolean>(false);

  function handleToggle() {
    setIsOpen(!isOpen);
  }

  return (
    <div>
      <IconButton type="button" onClick={handleToggle}>
        <MenuOpenIcon />
      </IconButton>
      <Drawer open={isOpen} onClose={() => setIsOpen(false)}>
        <CandidateSelector {...props} />
      </Drawer>
    </div>
  );
}

export default CandidateSelectorDrawer;
