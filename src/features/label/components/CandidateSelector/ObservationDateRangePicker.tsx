import { DateTimePicker } from '@mui/x-date-pickers';

import dayjs from '@/lib/dayjs';

import '@/features/label/styles/ObservationDataRangePicker.css';

interface ObservationDateRangePickerProps {
  startTime: dayjs.Dayjs;
  setStartTime: React.Dispatch<React.SetStateAction<dayjs.Dayjs>>;
  endTime: dayjs.Dayjs;
  setEndTime: React.Dispatch<React.SetStateAction<dayjs.Dayjs>>;
  fetchLatest: boolean;
}

function ObservationDateRangePicker({ ...props }: ObservationDateRangePickerProps) {
  /*
  const inlineStyling = {
    width: 220,
    '& .MuiInputBase-root': {
      color: 'rgba(255, 255, 255, 0.87)'
    },
    '& .MuiOutlinedInput-root': {
      '& .MuiOutlinedInput-notchedOutline': {
        borderColor: 'rgba(255, 255, 255, 0.87)'
      },
      '&:hover .MuiOutlinedInput-notchedOutline': {
        borderColor: '#cccccc',
        borderWidth: 2
      },
      '&.Mui-focused .MuiOutlinedInput-notchedOutline': {
        borderColor: '#646cff'
      },
      '& .MuiSvgIcon-root': {
        color: 'rgba(255, 255, 255, 0.87)'
      },
      '&:hover .MuiSvgIcon-root': {
        color: '#cccccc'
      },
      '&.Mui-focused .MuiSvgIcon-root': {
        color: '#646cff'
      }
    },
    '& .MuiInputLabel-root': {
      color: 'rgba(255, 255, 255, 0.87)',
      '&:hover': {
        color: '#cccccc'
      },
      '&.Mui-focused': {
        color: '#646cff'
      }
    }
  };
  */

  return (
    <div className="date-range-picker-container">
      <DateTimePicker
        label="Observation start time"
        value={props.startTime}
        onAccept={(newStartTime) => {
          if (newStartTime) {
            props.setStartTime(newStartTime);
          }
        }}
        disabled={props.fetchLatest}
        timezone="UTC"
        views={['year', 'month', 'day', 'hours', 'minutes', 'seconds']}
        ampm={false} // 24 hour clock
        format="YYYY-MM-DD HH:mm:ss"
      />
      <DateTimePicker
        label="Observation end time"
        value={props.endTime}
        onAccept={(newEndTime) => {
          if (newEndTime) {
            props.setEndTime(newEndTime);
          }
        }}
        disabled={props.fetchLatest}
        timezone="UTC"
        views={['year', 'month', 'day', 'hours', 'minutes', 'seconds']}
        ampm={false} // 24 hour clock
        format="YYYY-MM-DD HH:mm:ss"
      />
    </div>
  );
}

export default ObservationDateRangePicker;
