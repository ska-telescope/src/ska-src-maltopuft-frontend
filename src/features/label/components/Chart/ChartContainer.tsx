import { UseQueryResult } from '@tanstack/react-query';
import { useEffect, useState } from 'react';

import Chart from './Chart';

import { useEntities } from '@/features/label/api/getEntities';
import { useSubplots } from '@/features/label/api/getSubplot';
import { basePlotData, basePlotLayout } from '@/features/label/config';
import {
  AxisValues,
  Entity,
  Label,
  Observation,
  SinglePulse,
  SubplotData,
  ObservationSources
} from '@/features/label/types';

interface ChartContainerProps {
  labelsAssigned: Label[];
  setSelection: React.Dispatch<React.SetStateAction<number[]>>;
  labelsQuery: UseQueryResult<Label[]>;
  observationsQuery: UseQueryResult<Observation[]>;
  singlePulseQuery: UseQueryResult<SinglePulse[]>;
  observationRegionKnownPulsarsQuery: UseQueryResult<ObservationSources[]>;
  isSelectAll: boolean;
  setVisibleSourceIds: React.Dispatch<React.SetStateAction<number[]>>;
  zoomedAxisRange: AxisValues;
  setZoomedAxisRange: React.Dispatch<React.SetStateAction<AxisValues>>;
  setVisibleCandidateIds: React.Dispatch<React.SetStateAction<number[]>>;
}

function ChartContainer({ ...props }: ChartContainerProps) {
  const [chartLayout, setChartLayout] = useState<Partial<Plotly.Layout>>(basePlotLayout);
  const [hoverPoint, setHoverPoint] = useState<Plotly.PlotHoverEvent | null>(null);

  const {
    observationRegionKnownPulsarsQuery,
    observationsQuery,
    singlePulseQuery,
    setVisibleSourceIds,
    setVisibleCandidateIds,
    zoomedAxisRange
  } = props;

  const entitiesQuery = useEntities();
  const subplotsQuery = useSubplots(singlePulseQuery.data ?? []);

  /**
   * Merges the assigned and fetched labels to generate plot data.
   * Assigned labels take precedence over fetched labels, so elements are removed from
   * labelsFetched if they are present in labelsAssigned.
   * @param labelsAssigned Labels assigned to candidates in the interactive session
   * @param labelsFetched Candidate labels fetched with the MALTOPUFT API
   * @returns The union of fetched and assigned labels
   */
  function getPlotLabels(labelsAssigned: Label[], labelsFetched: Label[]): Label[] {
    if (labelsAssigned.length <= 0 && labelsFetched.length <= 0) {
      return [];
    }

    const assignedIds = new Set(labelsAssigned.map((label: Label) => label.candidate_id));
    const filteredLabelsFetched = labelsFetched.filter(
      (fetched: Label) => !assignedIds.has(fetched.candidate_id)
    );
    return [...labelsAssigned, ...filteredLabelsFetched];
  }

  /**
   * For an entities array (length N) and a labels array (length M), creates
   * a new array of length N which contains the entity objects with the
   * `labels` attribute.
   * @param entities An array of Entity objects
   * @param labels An array of Label objects for candidates which have been
   * labelled.
   * @returns An array of objects of type { ...Entity, labels: Label[]}
   */
  function entitiesWithLabels(entities: Entity[], labels: Label[], data: SinglePulse[]) {
    const labelCandidateIds = new Set(labels.map((label: Label) => label.candidate_id));

    const unlabelledEntity: Entity = { id: 0, css_color: '037ef2', type: 'UNLABELLED' };
    const unlabelledEntityData = {
      ...unlabelledEntity,
      labels: data
        .filter((sp: SinglePulse) => !labelCandidateIds.has(sp.candidate_id))
        .map((sp: SinglePulse) => ({
          candidate_id: sp.candidate_id,
          entity_id: unlabelledEntity.id
        }))
    };

    const labelledEntitiesData = entities.map((entity: Entity) => ({
      ...entity,
      labels: labels.filter((label: Label) => label.entity_id === entity.id)
    }));

    return [unlabelledEntityData, ...labelledEntitiesData];
  }

  /**
   * Transforms an array of SinglePulse objects to an array of
   * SinglePulse objects with the same ordering as the array of Label
   * objects
   * @param labels An array of Label objects
   * @param data An array of SinglePulse objects
   * @returns An array of SinglePulse objects
   */
  function mapToSinglePulseArray(labels: Label[], data: SinglePulse[]): SinglePulse[] {
    const dataMap = new Map<number, SinglePulse>();
    data.forEach((sp: SinglePulse) => dataMap.set(sp.candidate_id, sp));
    return labels
      .map((label) => dataMap.get(label.candidate_id))
      .filter((value): value is SinglePulse => value !== undefined);
  }

  /**
   * For an entities array (length N), a labels array (length M) and a data
   * array (length P), creates a new array of length N which contains the
   * entity objects with the `labels` and `candidates` attributes.
   * @param entities An array of Entity objects
   * @param labels An array of Label objects for candidates which have been
   * labelled.
   * @param data An array of SinglePulse objects
   * @returns An array of objects of type
   * { ...Entity, labels: Label[], candidates: SinglePulse[]}
   */
  function entitiesWithLabelsAndCandidates(
    entities: Entity[],
    labels: Label[],
    data: SinglePulse[]
  ) {
    const entityAndLabel = entitiesWithLabels(entities, labels, data);
    return entityAndLabel.map((entity) => ({
      ...entity,
      candidates: mapToSinglePulseArray.apply(data, [entity.labels as Label[], data])
    }));
  }

  /**
   * Creates the Plotly.Chart data prop, which expects an array of
   * Plotly.PlotData objects. Each object contains the data for a
   * unique entity, allowing them to be styled differently etc.
   * @param entities The objects that can be assigned as labels
   * @param labelsAssigned Labels assigned to candidates in the interactive session
   * @param labelsFetched Labels fetched from the MALTOPUFT API
   * @param data Single pulses fetched from the MALTOPUFT API
   * @returns An array of data to be rendered by the Plotly chart
   */
  function useChartData(
    entities: Entity[],
    labelsAssigned: Label[],
    labelsFetched: Label[],
    data: SinglePulse[]
  ): Partial<Plotly.PlotData>[] {
    // Render empty chart if there's no data
    if (data.length === 0) {
      return [
        {
          ...basePlotData,
          x: [],
          y: []
        }
      ];
    }

    const plotData: Partial<Plotly.PlotData>[] = [];
    const labels = getPlotLabels(labelsAssigned, labelsFetched);
    const entitiesWithData = entitiesWithLabelsAndCandidates(entities, labels, data);

    entitiesWithData.forEach((entity) => {
      // Don't add the data to the Plotly.PlotData array if there's no data
      // for the given entity
      if (entity.labels.length === 0 || entity.candidates.length === 0) {
        return null;
      }

      plotData.push({
        ...basePlotData,
        name: entity.type,
        x: entity.candidates.map((sp: SinglePulse) => sp.candidate.observed_at),
        y: entity.candidates.map((sp: SinglePulse) => sp.candidate.dm),
        customdata: entity.candidates.map((sp: SinglePulse) => sp.candidate_id),
        marker: {
          ...basePlotData.marker,
          color: entity.css_color,
          opacity: props.isSelectAll ? 0.3 : 0.75
        }
      });

      return plotData;
    });

    return plotData;
  }

  /**
   * Fetch the subplot for the data point being hovered over
   * @returns The base64 encoded subplot image data
   */
  function getSubplot(): string | undefined {
    if (hoverPoint === null) {
      return undefined;
    }
    const hoverCandidateId = hoverPoint.points[0].customdata;

    if (subplotsQuery.isSuccess) {
      const hoverCandidateData = subplotsQuery.data.find(
        (subplot: SubplotData) => subplot.candidate_id === hoverCandidateId
      );
      if (hoverCandidateData !== undefined) {
        return hoverCandidateData.imageData;
      }
    }

    return undefined;
  }

  /**
   * Finds the minimum and maximum values of the DM(t) plot axes
   * for the single pulses.
   * @returns The minimum x and y axis values of plotted data
   */
  function getSinglePulseDataRange(sps: SinglePulse[]): AxisValues {
    return sps.reduce<AxisValues>(
      (acc, curr) => {
        if (!acc.xMin || curr.candidate.observed_at < acc.xMin) {
          acc.xMin = curr.candidate.observed_at;
        }
        if (!acc.xMax || curr.candidate.observed_at > acc.xMax) {
          acc.xMax = curr.candidate.observed_at;
        }
        if (!acc.yMin || curr.candidate.dm < acc.yMin) {
          acc.yMin = curr.candidate.dm;
        }
        if (!acc.yMax || curr.candidate.dm > acc.yMax) {
          acc.yMax = curr.candidate.dm;
        }

        return acc;
      },
      {
        xMin: 0,
        xMax: 0,
        yMin: 0,
        yMax: 0
      }
    );
  }

  const chartData = useChartData(
    entitiesQuery.data ?? [],
    props.labelsAssigned,
    props.labelsQuery.data ?? [],
    props.singlePulseQuery.data ?? []
  );

  // Find visible sources
  useEffect(() => {
    /**
     * Finds the observation IDs that are in the axis ranges.
     * @param axisValues The minimum and maximum values of the DM(t) plot axes
     * @returns Observation IDs that are in the axis ranges.
     */
    function filterObsIdsInView(observations: Observation[], axisValues: AxisValues): number[] {
      return observations
        .filter((obs: Observation) => obs.t_min <= axisValues.xMax && obs.t_max >= axisValues.xMin)
        .map((obs: Observation) => obs.id);
    }

    /**
     * Finds the known pulsar sources that are in the axis ranges.
     * @param obsSources The known pulsar sources and their observations.
     * @param axisValues The minimum and maximum values of the DM(t) plot axes.
     * @returns Array of visible known pulsar sources.
     */
    function filterSourcesInView(
      observations: Observation[],
      obsSources: ObservationSources[],
      axisValues: AxisValues
    ): ObservationSources[] {
      const obsIds = filterObsIdsInView(observations, axisValues);

      if (obsIds.length === 0) {
        return [];
      }

      return obsSources
        .filter(
          (payload: ObservationSources) =>
            payload.source.dm !== null &&
            payload.observations.some(
              (obs: Observation) => obs.t_min <= axisValues.xMax && obs.t_max >= axisValues.xMin
            )
        )
        .map((payload: ObservationSources) => ({
          source: payload.source,
          observations: payload.observations
        }));
    }

    /**
     * Creates the known pulsar shapes that should be plotted on the chart.
     * @param axisValues The minimum and maximum values of data plotted on the DM(t) plot axes.
     * @param obsSources The known pulsar sources and their observations that are inside the axis range.
     * @returns The Plotly.Layout.shapes attribute for the visible known pulsar sources.
     */
    function prepareKnownPulsarShapes(
      axisValues: AxisValues,
      obsSources: ObservationSources[]
    ): Partial<Plotly.Shape>[] {
      if (obsSources.length === 0) {
        return [];
      }

      const shapes: Partial<Plotly.Shape>[] = [];

      obsSources.forEach((payload: ObservationSources) => {
        payload.observations.forEach((obs: Observation) => {
          shapes.push({
            type: 'line',
            // Maximum of xMin and t_min
            x0: axisValues.xMin > obs.t_min ? axisValues.xMin : obs.t_min,
            // Minimum of xMax and t_max
            x1: axisValues.xMax < obs.t_max ? axisValues.xMax : obs.t_max,
            y0: payload.source.dm,
            y1: payload.source.dm,
            line: {
              color: '#a81963',
              width: 3,
              dash: 'dot'
            },
            label: {
              text: payload.source.name,
              font: { size: 12, color: '#a81963' },
              textposition: 'end'
            }
          });
        });
      });
      return shapes;
    }

    /**
     * If a known pulsar payload exists, update the Plotly.Layout object passed
     * to the Plotly.Chart component with the known pulsar shapes.
     * @returns A Plotly.Layout object updated with known pulsar shapes.
     */
    function prepareLayout(
      axisValues: AxisValues,
      obsSources: ObservationSources[]
    ): Partial<Plotly.Layout> {
      const layout = { ...basePlotLayout };

      if (obsSources.length === 0) {
        return layout;
      }

      layout.shapes = prepareKnownPulsarShapes(axisValues, obsSources);
      return layout;
    }

    if (
      !observationsQuery.isSuccess ||
      !singlePulseQuery.isSuccess ||
      !observationRegionKnownPulsarsQuery.isSuccess
    ) {
      return;
    }

    const axisValues = getSinglePulseDataRange(singlePulseQuery.data);
    const visibleSources = filterSourcesInView(
      observationsQuery.data,
      observationRegionKnownPulsarsQuery.data,
      axisValues
    );

    setVisibleSourceIds(visibleSources.map((source) => source.source.id));
    setChartLayout(prepareLayout(axisValues, visibleSources));
  }, [
    observationRegionKnownPulsarsQuery.isSuccess,
    observationRegionKnownPulsarsQuery.data,
    singlePulseQuery.isSuccess,
    singlePulseQuery.data,
    observationsQuery.isSuccess,
    observationsQuery.data,
    setVisibleSourceIds
  ]);

  // Find visible candidates
  useEffect(() => {
    /**
     * Finds the candidate IDs that are in the axis ranges.
     * @param axisValues The minimum and maximum values of the DM(t) plot axes
     * @returns Candidate IDs that are in the axis ranges.
     */
    function filterCandidateIdsInView(
      singlePulses: SinglePulse[],
      axisValues: AxisValues
    ): number[] {
      if (
        axisValues.xMax === 0 ||
        axisValues.xMin === 0 ||
        axisValues.yMax === 0 ||
        axisValues.yMin === 0
      ) {
        return singlePulses.map((sp: SinglePulse) => sp.candidate_id);
      }

      return singlePulses
        .filter(
          (sp: SinglePulse) =>
            new Date(sp.candidate.observed_at) <= axisValues.xMax &&
            new Date(sp.candidate.observed_at) >= axisValues.xMin &&
            sp.candidate.dm <= axisValues.yMax &&
            sp.candidate.dm >= axisValues.yMin
        )
        .map((sp: SinglePulse) => sp.candidate_id);
    }

    if (!singlePulseQuery.isSuccess) {
      return;
    }

    setVisibleCandidateIds(filterCandidateIdsInView(singlePulseQuery.data, zoomedAxisRange));
  }, [singlePulseQuery.isSuccess, singlePulseQuery.data, zoomedAxisRange, setVisibleCandidateIds]);

  return (
    <Chart
      data={chartData}
      layout={chartLayout}
      setSelection={props.setSelection}
      setHoverPoint={setHoverPoint}
      subplot={getSubplot()}
      zoomedAxisRange={props.zoomedAxisRange}
      setZoomedAxisRange={props.setZoomedAxisRange}
    />
  );
}

export default ChartContainer;
