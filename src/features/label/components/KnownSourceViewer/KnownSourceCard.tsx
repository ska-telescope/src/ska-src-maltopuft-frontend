import Box from '@mui/material/Box';
import Card from '@mui/material/Card';
import Typography from '@mui/material/Typography';

import { KnownPulsar } from '@/features/label/types';

interface KnownSourceCardProps {
  knownPulsar: KnownPulsar | undefined;
}

function KnownSourceCard({ ...props }: KnownSourceCardProps) {
  return (
    props.knownPulsar && (
      <Box>
        <Card variant="outlined" style={{ width: '160px', padding: '10px' }}>
          <Typography variant="h6">{props.knownPulsar.name}</Typography>
          <a>
            <Typography variant="body2">{`DM: ${props.knownPulsar.dm}`}</Typography>
            <Typography variant="body2">{`Width: ${props.knownPulsar.width}`}</Typography>
            <Typography variant="body2">{`RA: ${props.knownPulsar.ra}`}</Typography>
            <Typography variant="body2">{`Dec: ${props.knownPulsar.dec}`}</Typography>
            <Typography variant="body2">{`Period: ${props.knownPulsar.period.toFixed(5)}`}</Typography>
          </a>
        </Card>
      </Box>
    )
  );
}

export default KnownSourceCard;
