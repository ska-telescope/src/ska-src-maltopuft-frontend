import KnownSourceCard from './KnownSourceCard';

import { KnownPulsar } from '@/features/label/types';

import '@/features/label/styles/KnownSourceCardContainer.css';

interface KnownSourceCardContainerProps {
  visibleSourceIds: number[];
  sources: KnownPulsar[];
}

function KnownSourceCardContainer({ ...props }: KnownSourceCardContainerProps) {
  if (props.sources.length === 0 || props.visibleSourceIds.length === 0) {
    return null;
  }

  return (
    <div className="known-source-card-container">
      {props.sources
        .filter((pulsar: KnownPulsar) => props.visibleSourceIds.includes(pulsar.id))
        .map((pulsar: KnownPulsar) => (
          <KnownSourceCard key={pulsar.id} knownPulsar={pulsar} />
        ))}
    </div>
  );
}

export default KnownSourceCardContainer;
