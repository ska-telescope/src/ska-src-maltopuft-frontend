import { UseQueryResult } from '@tanstack/react-query';

import LabelButtonContainer from './LabelButtonContainer';
import PostLabelButton from './PostLabelButton';

import { Label, SinglePulse } from '@/features/label/types';

import '@/features/label/styles/LabelBar.css';

interface LabelBarProps {
  labelsAssigned: Label[];
  setLabelsAssigned: React.Dispatch<React.SetStateAction<Label[]>>;
  selection: number[];
  setSelection: React.Dispatch<React.SetStateAction<number[]>>;
  labelsQuery: UseQueryResult<Label[]>;
  singlePulseQuery: UseQueryResult<SinglePulse[]>;
  isSelectAll: boolean;
  setIsSelectAll: React.Dispatch<React.SetStateAction<boolean>>;
}

function LabelBar({ ...props }: LabelBarProps) {
  return (
    <div className="sp-label-bar">
      <PostLabelButton
        labelsAssigned={props.labelsAssigned}
        setLabelsAssigned={props.setLabelsAssigned}
        labelsQuery={props.labelsQuery}
        isSelectAll={props.isSelectAll}
        setIsSelectAll={props.setIsSelectAll}
        setSelection={props.setSelection}
      />
      <LabelButtonContainer
        labelsAssigned={props.labelsAssigned}
        setLabelsAssigned={props.setLabelsAssigned}
        selection={props.selection}
      />
    </div>
  );
}

export default LabelBar;
