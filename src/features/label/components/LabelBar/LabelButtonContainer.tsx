import LabelButton from './LabelButton';

import { useEntities } from '@/features/label/api/getEntities';
import { Entity, Label } from '@/features/label/types';

interface LabelButtonsContainerProps {
  labelsAssigned: Label[];
  setLabelsAssigned: React.Dispatch<React.SetStateAction<Label[]>>;
  selection: number[];
}

function LabelButtonContainer({ ...props }: LabelButtonsContainerProps) {
  const entitiesQuery = useEntities();

  if (entitiesQuery.isLoading) {
    return <p>Loading...</p>;
  }
  if (entitiesQuery.isSuccess) {
    return entitiesQuery.data
      .filter((ent: Entity) => ent.type !== 'UNLABELLED')
      .map((ent: Entity) => (
        <LabelButton
          key={ent.id}
          entityId={ent.id}
          type={ent.type}
          labelsAssigned={props.labelsAssigned}
          setLabelsAssigned={props.setLabelsAssigned}
          selection={props.selection}
        />
      ));
  }

  return null;
}

export default LabelButtonContainer;
