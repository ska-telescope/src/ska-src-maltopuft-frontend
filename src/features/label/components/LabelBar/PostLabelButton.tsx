import SaveIcon from '@mui/icons-material/Save';
import { IconButton } from '@mui/material';
import { UseQueryResult } from '@tanstack/react-query';

import { useCreateLabels } from '@/features/label/api/createLabels';
import { useUpdateLabel } from '@/features/label/api/updateLabels';
import { Label } from '@/features/label/types';
import { queryClient } from '@/lib/react-query';

interface PostLabelButtonProps {
  labelsAssigned: Label[];
  setLabelsAssigned: React.Dispatch<React.SetStateAction<Label[]>>;
  labelsQuery: UseQueryResult<Label[]>;
  isSelectAll: boolean;
  setIsSelectAll: React.Dispatch<React.SetStateAction<boolean>>;
  setSelection: React.Dispatch<React.SetStateAction<number[]>>;
}

function PostLabelButton({ ...props }: PostLabelButtonProps) {
  const createLabelsMutation = useCreateLabels();
  const updateLabelMutation = useUpdateLabel();

  function getRelabelled(labelsAssigned: Label[], labelsFetched: Label[]): Label[] {
    const labelsFetchedMap = new Map(
      labelsFetched.map((label: Label) => [label.candidate_id, label.id])
    );

    return labelsAssigned
      .filter((assigned: Label) => labelsFetchedMap.has(assigned.candidate_id))
      .map((assigned: Label) => ({
        ...assigned,
        id: labelsFetchedMap.get(assigned.candidate_id)
      }));
  }

  function removeLabelsFromState(labelsToRemove: Label[]): void {
    props.setLabelsAssigned((prevAssigned) =>
      prevAssigned.filter(
        (assigned: Label) =>
          !labelsToRemove.some((label: Label) => label.candidate_id === assigned.candidate_id)
      )
    );
  }

  async function handleUpdate(labelsToUpdate: Label[]): Promise<void> {
    const updatedLabels = await Promise.all(
      labelsToUpdate.map((updated: Label) => updateLabelMutation.mutateAsync(updated))
    );

    // After updating all labels, invalidate the labels query to trigger a refetch
    await queryClient.invalidateQueries({ queryKey: ['labels', 'get'] });

    removeLabelsFromState(updatedLabels);
  }

  function handleCreate(labelsToCreate: Label[]): void {
    createLabelsMutation.mutate(labelsToCreate, {
      onSuccess: () => {
        removeLabelsFromState(labelsToCreate);
      }
    });
  }

  async function handleClick(): Promise<void> {
    if (props.labelsAssigned.length === 0) {
      return;
    }

    // First, handle label updates
    const labelsToUpdate = getRelabelled(props.labelsAssigned, props.labelsQuery?.data ?? []);

    if (labelsToUpdate.length > 0) {
      await handleUpdate(labelsToUpdate);
    }

    const labelsToCreate = props.labelsAssigned.filter(
      (assigned: Label) =>
        !labelsToUpdate.some((updated: Label) => updated.candidate_id === assigned.candidate_id)
    );

    if (labelsToCreate.length > 0) {
      handleCreate(labelsToCreate);
    }
    if (props.isSelectAll) {
      props.setIsSelectAll(false);
      props.setSelection([]);
    }
  }

  return (
    <IconButton
      className="button"
      type="button"
      onClick={handleClick}
      disabled={
        createLabelsMutation.isPending ||
        !props.labelsQuery.isSuccess ||
        props.labelsAssigned.length === 0
      }
      sx={{
        borderRadius: 0,
        '&:hover': {
          border: '1px solid transparent',
          borderColor: '#646cff',
          borderStyle: 'solid'
        },
        '&.Mui-disabled': {
          '& .MuiSvgIcon-root': {
            color: '#a0a0a0'
          }
        }
      }}
    >
      <SaveIcon sx={{ color: '#cccccc' }} />
    </IconButton>
  );
}

export default PostLabelButton;
