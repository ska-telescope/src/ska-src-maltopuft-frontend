import { UseQueryResult } from '@tanstack/react-query';

import SelectAllButton from './SelectAllButton';

import CandidateSelectorDrawer from '@/features/label/components/CandidateSelector/CandidateSelectorDrawer';
import LabelBar from '@/features/label/components/LabelBar/LabelBar';
import { Label, SinglePulse } from '@/features/label/types';
import dayjs from '@/lib/dayjs';

import '@/features/label/styles/MenuBar.css';

interface MenuBarProps {
  labelsAssigned: Label[];
  setLabelsAssigned: React.Dispatch<React.SetStateAction<Label[]>>;
  selection: number[];
  setSelection: React.Dispatch<React.SetStateAction<number[]>>;
  isSelectAll: boolean;
  setIsSelectAll: React.Dispatch<React.SetStateAction<boolean>>;
  labelsQuery: UseQueryResult<Label[]>;
  singlePulseQuery: UseQueryResult<SinglePulse[]>;
  fetchLatest: boolean;
  setFetchLatest: React.Dispatch<React.SetStateAction<boolean>>;
  startTime: dayjs.Dayjs;
  setStartTime: React.Dispatch<React.SetStateAction<dayjs.Dayjs>>;
  endTime: dayjs.Dayjs;
  setEndTime: React.Dispatch<React.SetStateAction<dayjs.Dayjs>>;
  visibleCandidateIds: number[];
}

function MenuBar({ ...props }: MenuBarProps) {
  return (
    <div className="sp-menu-bar">
      <CandidateSelectorDrawer
        fetchLatest={props.fetchLatest}
        setFetchLatest={props.setFetchLatest}
        startTime={props.startTime}
        setStartTime={props.setStartTime}
        endTime={props.endTime}
        setEndTime={props.setEndTime}
      />
      <LabelBar
        labelsAssigned={props.labelsAssigned}
        setLabelsAssigned={props.setLabelsAssigned}
        selection={props.selection}
        setSelection={props.setSelection}
        labelsQuery={props.labelsQuery}
        singlePulseQuery={props.singlePulseQuery}
        isSelectAll={props.isSelectAll}
        setIsSelectAll={props.setIsSelectAll}
      />
      <SelectAllButton
        isSelectAll={props.isSelectAll}
        setIsSelectAll={props.setIsSelectAll}
        setSelection={props.setSelection}
        singlePulseQuery={props.singlePulseQuery}
        visibleCandidateIds={props.visibleCandidateIds}
      />
    </div>
  );
}

export default MenuBar;
