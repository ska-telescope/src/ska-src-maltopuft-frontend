import { UseQueryResult } from '@tanstack/react-query';

import { SinglePulse } from '@/features/label/types';

interface SelectAllButtonProps {
  setSelection: React.Dispatch<React.SetStateAction<number[]>>;
  singlePulseQuery: UseQueryResult<SinglePulse[]>;
  isSelectAll: boolean;
  setIsSelectAll: React.Dispatch<React.SetStateAction<boolean>>;
  visibleCandidateIds: number[];
}

function SelectAllButton({ ...props }: SelectAllButtonProps) {
  function handleClick(): void {
    props.setIsSelectAll(!props.isSelectAll);

    if (
      props.isSelectAll ||
      !props.singlePulseQuery.isSuccess ||
      props.singlePulseQuery.data.length === 0
    ) {
      props.setSelection([]);
      return;
    }

    props.setSelection([...props.visibleCandidateIds]);
  }

  return (
    <button type="button" onClick={handleClick}>
      {props.isSelectAll ? 'Deselect all' : 'Select all'}
    </button>
  );
}

export default SelectAllButton;
