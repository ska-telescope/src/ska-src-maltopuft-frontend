import { Select, MenuItem, SelectChangeEvent } from '@mui/material';

interface PageSizeSelectorProps {
  pageSize: number;
  setPageSize: React.Dispatch<React.SetStateAction<number>>;
  setPageNumber: React.Dispatch<React.SetStateAction<number>>;
}

function PageSizeSelector({ ...props }: PageSizeSelectorProps) {
  function handleChange(event: SelectChangeEvent<number>) {
    props.setPageSize(Number(event.target.value));

    // If the page size changes, reset the page number to 0
    props.setPageNumber(0);
  }

  return (
    <div className="sp-pagination-size-selector">
      <Select
        labelId="page-size-select-label"
        name="pageSize"
        value={props.pageSize}
        label="Page Size"
        onChange={handleChange}
      >
        <MenuItem value={10}>10</MenuItem>
        <MenuItem value={20}>20</MenuItem>
        <MenuItem value={50}>50</MenuItem>
        <MenuItem value={100}>100</MenuItem>
        <MenuItem value={250}>250</MenuItem>
        <MenuItem value={500}>500</MenuItem>
      </Select>
    </div>
  );
}

export default PageSizeSelector;
