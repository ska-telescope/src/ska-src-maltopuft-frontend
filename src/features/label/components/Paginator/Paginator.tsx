import { UseQueryResult } from '@tanstack/react-query';

import PageSizeSelector from './PageSizeSelector';
import PaginationButton from './PaginationButton';

import { SinglePulse } from '@/features/label/types';

import '@/features/label/styles/Paginator.css';

interface PaginatorProps {
  pageSize: number;
  setPageSize: React.Dispatch<React.SetStateAction<number>>;
  pageNumber: number;
  setPageNumber: React.Dispatch<React.SetStateAction<number>>;
  singlePulseCount: UseQueryResult<number>;
  singlePulseQuery: UseQueryResult<SinglePulse[]>;
}

function Paginator({ ...props }: PaginatorProps) {
  return (
    <div className="sp-paginator-container">
      <PaginationButton
        pageNumber={props.pageNumber}
        setPageNumber={props.setPageNumber}
        pageSize={props.pageSize}
        singlePulseCount={props.singlePulseCount}
        singlePulseQuery={props.singlePulseQuery}
      />
      <PageSizeSelector
        pageSize={props.pageSize}
        setPageSize={props.setPageSize}
        setPageNumber={props.setPageNumber}
      />
    </div>
  );
}

export default Paginator;
