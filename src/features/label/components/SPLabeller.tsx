import { useState, useEffect } from 'react';

import ChartContainer from './Chart/ChartContainer';
import KnownSourceCardContainer from './KnownSourceViewer/KnownSourceCardContainer';
import MenuBar from './MenuBar/MenuBar';
import Paginator from './Paginator/Paginator';

import { useSinglePulseCount } from '@/features/label/api/countSinglePulses';
import { useLabels } from '@/features/label/api/getLabels';
import { useObservationRegionKnownPulsars } from '@/features/label/api/getObservationRegionKnownPulsars';
import { useObservations } from '@/features/label/api/getObservations';
import { useSinglePulses } from '@/features/label/api/getSinglePulses';
import {
  AxisValues,
  Label,
  Observation,
  ObservationSources,
  SinglePulse
} from '@/features/label/types';
import dayjs from '@/lib/dayjs';

import '@/features/label/styles/SPLabeller.css';

/**
 * SPLabeller Component
 *
 * Responsible for labeling single pulse candidates with 'RFI', 'Single pulse'
 * or 'Periodic pulse' labels.
 *
 * It provides controls for fetching data based on observation time interval,
 * setting observation date ranges, label assignment, and paginating through
 * large single pulse datasets.
 */
function SPLabeller() {
  const dataStartUTC = dayjs('2023-11-12').utc().startOf('day');
  const todayStartUTC = dayjs().utc().startOf('day');

  /* State */
  const [pageSize, setPageSize] = useState<number>(100);
  const [pageNumber, setPageNumber] = useState<number>(0);
  const [fetchLatest, setFetchLatest] = useState<boolean>(false);
  const [labelsAssigned, setLabelsAssigned] = useState<Label[]>([]);
  const [selection, setSelection] = useState<number[]>([]);
  const [startTime, setStartTime] = useState<dayjs.Dayjs>(dataStartUTC);
  const [endTime, setEndTime] = useState<dayjs.Dayjs>(todayStartUTC.add(1, 'day'));
  const [isSelectAll, setIsSelectAll] = useState<boolean>(false);
  const [visibleSourceIds, setVisibleSourceIds] = useState<number[]>([]);
  const [visibleCandidateIds, setVisibleCandidateIds] = useState<number[]>([]);
  const [zoomedAxisRange, setZoomedAxisRange] = useState<AxisValues>({
    xMin: 0,
    xMax: 0,
    yMin: 0,
    yMax: 0
  });

  /* UseQueryResult objects */
  const observationsQuery = useObservations(startTime, endTime);

  const observationIds = observationsQuery.isSuccess
    ? observationsQuery.data.map((obs: Observation) => obs.id)
    : [];

  const radius = 0.1;
  const observationRegionKnownPulsarsQuery = useObservationRegionKnownPulsars(
    observationIds,
    radius
  );

  const singlePulseQuery = useSinglePulses(pageNumber, pageSize, fetchLatest, observationIds);
  const singlePulseCount = useSinglePulseCount(observationIds, fetchLatest);
  const singlePulseCandidateIds = singlePulseQuery.isSuccess
    ? singlePulseQuery.data.map((sp: SinglePulse) => sp.candidate_id)
    : [];

  const labelsQuery = useLabels(singlePulseCandidateIds);

  // Set pageNumber to 0 when new candidate data is fetched
  useEffect(() => {
    setPageNumber(0);
  }, [observationsQuery.data, fetchLatest]);

  // Deselect all if new data is fetched, page number is changed or axis scaled
  useEffect(() => {
    setIsSelectAll(false);
    setSelection([]);
  }, [observationsQuery.data, fetchLatest, pageNumber, pageSize, zoomedAxisRange]);

  return (
    <div className="sp-labeller-container">
      <div className="sp-chart-container">
        <MenuBar
          labelsAssigned={labelsAssigned}
          setLabelsAssigned={setLabelsAssigned}
          selection={selection}
          setSelection={setSelection}
          isSelectAll={isSelectAll}
          setIsSelectAll={setIsSelectAll}
          labelsQuery={labelsQuery}
          singlePulseQuery={singlePulseQuery}
          fetchLatest={fetchLatest}
          setFetchLatest={setFetchLatest}
          startTime={startTime}
          setStartTime={setStartTime}
          endTime={endTime}
          setEndTime={setEndTime}
          visibleCandidateIds={visibleCandidateIds}
        />
        <ChartContainer
          labelsAssigned={labelsAssigned}
          setSelection={setSelection}
          labelsQuery={labelsQuery}
          observationsQuery={observationsQuery}
          singlePulseQuery={singlePulseQuery}
          observationRegionKnownPulsarsQuery={observationRegionKnownPulsarsQuery}
          isSelectAll={isSelectAll}
          setVisibleSourceIds={setVisibleSourceIds}
          setVisibleCandidateIds={setVisibleCandidateIds}
          zoomedAxisRange={zoomedAxisRange}
          setZoomedAxisRange={setZoomedAxisRange}
        />
        <div className="sp-labeller-rh-toolbar">
          <KnownSourceCardContainer
            visibleSourceIds={visibleSourceIds}
            sources={
              observationRegionKnownPulsarsQuery?.data?.map(
                (sourceObs: ObservationSources) => sourceObs.source
              ) ?? []
            }
          />
          <Paginator
            pageSize={pageSize}
            setPageSize={setPageSize}
            pageNumber={pageNumber}
            setPageNumber={setPageNumber}
            singlePulseCount={singlePulseCount}
            singlePulseQuery={singlePulseQuery}
          />
        </div>
      </div>
    </div>
  );
}

export default SPLabeller;
