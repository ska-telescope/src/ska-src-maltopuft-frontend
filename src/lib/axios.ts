import Axios, { AxiosError, AxiosResponse, InternalAxiosRequestConfig } from 'axios';

import { BACKEND_API_URL } from '@/config';
import { logout } from '@/features/auth/api/auth';
import { Token } from '@/features/auth/types';

/**
 * API axios instance configured to point to ``BACKEND_API_URL``
 */
export const api = Axios.create({
  baseURL: BACKEND_API_URL
});

/**
 * Adds an authentication header to request
 * @param config The request configuration
 * @returns The request congifuration with authentication headers
 */
function authRequestInterceptor(config: InternalAxiosRequestConfig) {
  config.headers.Accept = 'application/json';

  const token: string | null = localStorage.getItem('maltopuft-token');
  if (token === null) {
    return config;
  }

  const parsedToken = JSON.parse(token) as Token;
  if (parsedToken === null) {
    return config;
  }

  config.headers.authorization = `Bearer ${parsedToken.access_token}`;
  return config;
}

api.interceptors.request.use(authRequestInterceptor);

/**
 * Adds a response interceptor to the API axios instance
 * The response interceptor checks for unauthorised status codes and, if
 * present, logs the user out. If any other error is returned, the Promise
 * is rejected. Otherwise, the request is returned.
 */
api.interceptors.response.use(
  // If no error, just return the response
  (response: AxiosResponse) => response,
  (error: AxiosError) => {
    // Force logout if API returns unauthorised status code
    if (error?.response?.status === 401) {
      logout();
    }
    return Promise.reject(error);
  }
);

/**
 * Auth API axios instance configured to point to ${BACKEND_API_URL}/auth
 * with no request interceptor.
 */
export const authAPI = Axios.create({
  baseURL: `${BACKEND_API_URL}/auth`
});
