import { createTheme } from '@mui/material/styles';

export const theme = createTheme({
  palette: {
    mode: 'dark',
    background: {
      default: '#242424',
      paper: '#1a1a1a'
    },
    text: {
      primary: 'rgba(255, 255, 255, 0.87)'
    },
    primary: {
      main: '#646cff',
      light: '#535bf2'
    }
  },
  typography: {
    fontFamily: 'Inter, system-ui, Avenir, Helvetica, Arial, sans-serif',
    fontSize: 16,
    fontWeightRegular: 400,
    fontWeightMedium: 500,
    h1: {
      fontSize: '3.2em',
      lineHeight: 1.1
    }
  },
  components: {
    MuiButton: {
      styleOverrides: {
        root: {
          borderRadius: '8px',
          border: '1px solid transparent',
          padding: '0.6em 1.2em',
          fontSize: '1em',
          fontWeight: 500,
          backgroundColor: '#1a1a1a',
          transition: 'border-color 0.25s',
          '&:hover': {
            borderColor: '#646cff',
            backgroundColor: '#1a1a1a'
          },
          '&:focus': {
            outline: '4px auto -webkit-focus-ring-color'
          }
        }
      }
    },
    MuiLink: {
      styleOverrides: {
        root: {
          fontWeight: 500,
          color: '#646cff',
          textDecoration: 'inherit',
          '&:hover': {
            color: '#535bf2'
          }
        }
      }
    },
    MuiCssBaseline: {
      styleOverrides: {
        body: {
          margin: 0,
          display: 'flex',
          placeItems: 'center',
          minWidth: '320px',
          minHeight: '100vh',
          fontSynthesis: 'none',
          textRendering: 'optimizeLegibility',
          WebkitFontSmoothing: 'antialiased',
          MozOsxFontSmoothing: 'grayscale'
        }
      }
    }
  }
});
