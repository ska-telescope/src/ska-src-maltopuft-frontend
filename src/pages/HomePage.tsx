import { AUTH_DISABLED } from '@/config';
import { useAuthUrl } from '@/features/auth/api/auth';
import LoginButton from '@/features/auth/components/LoginButton';

function HomePage() {
  const useAuthUrlQuery = useAuthUrl();
  if (AUTH_DISABLED === false) {
    if (useAuthUrlQuery.isSuccess) {
      window.location.href = useAuthUrlQuery.data.auth_url;
    }
  }

  return (
    <div>
      <LoginButton />
      <p>
        Redirecting to login. If you are not automatically re-directed, please click the Login
        button above.
      </p>
    </div>
  );
}

export default HomePage;
