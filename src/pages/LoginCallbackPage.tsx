import { Navigate } from 'react-router-dom';

import { useToken } from '@/features/auth/api/fetchToken';

function LoginCallbackPage() {
  function getParamFromURL(param: string): string {
    const value = new URLSearchParams(window.location.search).get(param);
    if (value === null) {
      throw Error('Param not found in url.');
    }
    return value;
  }

  const code = getParamFromURL('code');
  const tokenQuery = useToken(code);
  if (tokenQuery.isSuccess) {
    localStorage.setItem('maltopuft-token', JSON.stringify(tokenQuery.data.token));
    return <Navigate to="/label" />;
  }

  return null;
}

export default LoginCallbackPage;
